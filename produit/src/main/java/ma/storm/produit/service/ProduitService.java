package ma.storm.produit.service;

import java.util.List;

import ma.storm.produit.model.Produit;

public interface ProduitService {

	Produit saveProduit(Produit Produit);

	List<Produit> saveProduits(List<Produit> Produits);

	List<Produit> getProduits();

	Produit getProduitById(long id);

	String deleteProduit(long id);

	Produit updateProduit(long id, Produit produit);

	Produit getProduitByName(String name);

}
