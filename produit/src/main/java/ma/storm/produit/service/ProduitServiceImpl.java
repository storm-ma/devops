package ma.storm.produit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.storm.produit.model.Produit;
import ma.storm.produit.repository.ProduitRepository;

@Service
public class ProduitServiceImpl implements ProduitService {
	
    @Autowired
    private ProduitRepository repository;

	@Override
    public Produit saveProduit(Produit Produit) {
        return repository.save(Produit);
    }

	@Override
    public List<Produit> saveProduits(List<Produit> Produits) {
        return repository.saveAll(Produits);
    }

	@Override
    public List<Produit> getProduits() {
        return repository.findAll();
    }

	@Override
    public Produit getProduitById(long id) {
        return repository.findById(id).orElse(null);
    }

	@Override
    public Produit getProduitByName(String name) {
        return repository.findByNomProduit(name);
    }

	@Override
    public String deleteProduit(long id) {
        repository.deleteById(id);
        return "Produit removed !! " + id;
    }

	@Override
    public Produit updateProduit(long id, Produit produit) {
        Produit existingProduit = repository.findById(id).orElse(null);
        existingProduit.setNomProduit(produit.getNomProduit());
        existingProduit.setQuantite(produit.getQuantite());
        existingProduit.setPrix(produit.getPrix());
        existingProduit.setDateCreation(produit.getDateCreation());
        return repository.save(existingProduit);
    }

}
