package ma.storm.produit.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProduitDTO {

	private long idProduit;
	private String nomProduit;
	@JsonFormat(pattern="dd/MM/yyyy")
	private LocalDate dateCreation;
	private Integer quantite;
	private double prix;
}
