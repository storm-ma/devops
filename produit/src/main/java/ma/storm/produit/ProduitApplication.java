package ma.storm.produit;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class ProduitApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(ProduitApplication.class).run(args);
	}

}
