package ma.storm.produit.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "PRODUIT")
public class Produit {
	
	@Id
	@Column(name = "id_produit")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idProduit;
	@Column(name = "nom_produit")
	private String nomProduit;
	@Column(name = "date_creation")
	private LocalDate dateCreation;
	@Column(name = "quantite")
	private Integer quantite;
	@Column(name = "prix")
	private double prix;
	
}
