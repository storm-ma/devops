package ma.storm.produit.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.storm.produit.model.Produit;

public interface ProduitRepository extends JpaRepository<Produit,Long> {

	Produit findByNomProduit(String name);
	
}
