package ma.storm.produit.mapper;

import org.mapstruct.Mapper;

import ma.storm.produit.dto.ProduitDTO;
import ma.storm.produit.model.Produit;

@Mapper(componentModel = "spring")
public interface ProduitMapper {
	
	Produit mapProduitDTOToProduit(ProduitDTO produitDTO);
	ProduitDTO mapProduitToProduitDTO(Produit produitDTO);
}