package ma.storm.produit.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ma.storm.produit.dto.ProduitDTO;
import ma.storm.produit.mapper.ProduitMapper;
import ma.storm.produit.service.ProduitService;

@RestController
public class ProduitController {

	@Autowired
    private ProduitService service;
	@Autowired
    private ProduitMapper mapper;

    @PostMapping("/produit")
    public ProduitDTO addProduit(@RequestBody ProduitDTO produit) {
        return mapper.mapProduitToProduitDTO(service.saveProduit(mapper.mapProduitDTOToProduit(produit)));
    }

    @PostMapping("/produits")
    public List<ProduitDTO> addProduits(@RequestBody List<ProduitDTO> produits) {
        return service.saveProduits(produits.stream().map(produitDTO -> mapper.mapProduitDTOToProduit(produitDTO)).collect(Collectors.toList())).stream().map(produit -> mapper.mapProduitToProduitDTO(produit)).collect(Collectors.toList());
    }

    @GetMapping("/produits")
    public List<ProduitDTO> findAllProduits() {
        return service.getProduits().stream().map(produit -> mapper.mapProduitToProduitDTO(produit)).collect(Collectors.toList());
    }

    @GetMapping("/produit/{id}")
    public ProduitDTO findProduitById(@PathVariable long id) {
        return mapper.mapProduitToProduitDTO(service.getProduitById(id));
    }

    @PutMapping("/produit/{id}")
    public ProduitDTO updateProduit(@PathVariable long id, @RequestBody ProduitDTO produit) {
        return mapper.mapProduitToProduitDTO(service.updateProduit(id, mapper.mapProduitDTOToProduit(produit)));
    }

    @DeleteMapping("/produit/{id}")
    public String deleteProduit(@PathVariable int id) {
        return service.deleteProduit(id);
    }
}
