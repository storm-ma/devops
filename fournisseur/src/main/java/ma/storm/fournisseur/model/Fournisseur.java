package ma.storm.fournisseur.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "FOURNISSEUR")
public class Fournisseur {
	
	@Id
	@Column(name = "id_fournisseur")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idFournisseur;
	@Column(name = "nom_fournisseur")
	private String nomFournisseur;
	@Column(name = "ville_fournisseur")
	private String villeFournisseur;
	
}
