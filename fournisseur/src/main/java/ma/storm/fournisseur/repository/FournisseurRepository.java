package ma.storm.fournisseur.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.storm.fournisseur.model.Fournisseur;

public interface FournisseurRepository extends JpaRepository<Fournisseur,Long> {

	Fournisseur findByNomFournisseur(String name);
	
}
