package ma.storm.fournisseur.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ma.storm.fournisseur.dto.FournisseurDTO;
import ma.storm.fournisseur.mapper.FournisseurMapper;
import ma.storm.fournisseur.service.FournisseurService;

@RestController
public class FournisseurController {

	@Autowired
    private FournisseurService service;
	@Autowired
    private FournisseurMapper mapper;

    @PostMapping("/fournisseur")
    public FournisseurDTO addFournisseur(@RequestBody FournisseurDTO fournisseur) {
        return mapper.mapFournisseurToFournisseurDTO(service.saveFournisseur(mapper.mapFournisseurDTOToFournisseur(fournisseur)));
    }

    @PostMapping("/fournisseurs")
    public List<FournisseurDTO> addFournisseurs(@RequestBody List<FournisseurDTO> fournisseurs) {
        return service.saveFournisseurs(fournisseurs.stream().map(fournisseurDTO -> mapper.mapFournisseurDTOToFournisseur(fournisseurDTO)).collect(Collectors.toList())).stream().map(fournisseur -> mapper.mapFournisseurToFournisseurDTO(fournisseur)).collect(Collectors.toList());
    }

    @GetMapping("/fournisseurs")
    public List<FournisseurDTO> findAllFournisseurs() {
        return service.getFournisseurs().stream().map(fournisseur -> mapper.mapFournisseurToFournisseurDTO(fournisseur)).collect(Collectors.toList());
    }

    @GetMapping("/fournisseur/{id}")
    public FournisseurDTO findFournisseurById(@PathVariable long id) {
        return mapper.mapFournisseurToFournisseurDTO(service.getFournisseurById(id));
    }

    @PutMapping("/fournisseur/{id}")
    public FournisseurDTO updateFournisseur(@PathVariable long id, @RequestBody FournisseurDTO fournisseur) {
        return mapper.mapFournisseurToFournisseurDTO(service.updateFournisseur(id, mapper.mapFournisseurDTOToFournisseur(fournisseur)));
    }

    @DeleteMapping("/fournisseur/{id}")
    public String deleteFournisseur(@PathVariable int id) {
        return service.deleteFournisseur(id);
    }
}
