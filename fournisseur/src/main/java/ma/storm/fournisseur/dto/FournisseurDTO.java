package ma.storm.fournisseur.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FournisseurDTO {

	private long idFournisseur;
	private String nomFournisseur;
	private String villeFournisseur;
}
