package ma.storm.fournisseur.mapper;

import org.mapstruct.Mapper;

import ma.storm.fournisseur.dto.FournisseurDTO;
import ma.storm.fournisseur.model.Fournisseur;

@Mapper(componentModel = "spring")
public interface FournisseurMapper {
	
	Fournisseur mapFournisseurDTOToFournisseur(FournisseurDTO fournisseurDTO);
	FournisseurDTO mapFournisseurToFournisseurDTO(Fournisseur fournisseurDTO);
}