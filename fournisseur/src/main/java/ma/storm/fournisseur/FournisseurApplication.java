package ma.storm.fournisseur;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class FournisseurApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(FournisseurApplication.class).run(args);
	}

}
