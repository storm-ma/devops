package ma.storm.fournisseur.service;

import java.util.List;

import ma.storm.fournisseur.model.Fournisseur;

public interface FournisseurService {

	Fournisseur saveFournisseur(Fournisseur Fournisseur);

	List<Fournisseur> saveFournisseurs(List<Fournisseur> Fournisseurs);

	List<Fournisseur> getFournisseurs();

	Fournisseur getFournisseurById(long id);

	String deleteFournisseur(long id);

	Fournisseur updateFournisseur(long id, Fournisseur fournisseur);

	Fournisseur getFournisseurByName(String name);

}
