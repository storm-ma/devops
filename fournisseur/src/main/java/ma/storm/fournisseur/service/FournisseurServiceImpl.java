package ma.storm.fournisseur.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.storm.fournisseur.model.Fournisseur;
import ma.storm.fournisseur.repository.FournisseurRepository;

@Service
public class FournisseurServiceImpl implements FournisseurService {
	
    @Autowired
    private FournisseurRepository repository;

	@Override
    public Fournisseur saveFournisseur(Fournisseur Fournisseur) {
        return repository.save(Fournisseur);
    }

	@Override
    public List<Fournisseur> saveFournisseurs(List<Fournisseur> Fournisseurs) {
        return repository.saveAll(Fournisseurs);
    }

	@Override
    public List<Fournisseur> getFournisseurs() {
        return repository.findAll();
    }

	@Override
    public Fournisseur getFournisseurById(long id) {
        return repository.findById(id).orElse(null);
    }

	@Override
    public Fournisseur getFournisseurByName(String name) {
        return repository.findByNomFournisseur(name);
    }

	@Override
    public String deleteFournisseur(long id) {
        repository.deleteById(id);
        return "Fournisseur removed !! " + id;
    }

	@Override
    public Fournisseur updateFournisseur(long id, Fournisseur fournisseur) {
        Fournisseur existingFournisseur = repository.findById(id).orElse(null);
        existingFournisseur.setNomFournisseur(fournisseur.getNomFournisseur());
        existingFournisseur.setVilleFournisseur(fournisseur.getVilleFournisseur());
        return repository.save(existingFournisseur);
    }

}
